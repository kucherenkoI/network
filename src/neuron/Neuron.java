package neuron;

/**
 * Created by kucherenko on 04.04.15.
 */

public class Neuron {

    private final String tag;

    public interface ActivationFunction {
        public double function(double y);
    }

    private double[] weights;
    private double[] preparedX;

    private double y;

    public Neuron(int inputCount, String tag) {
        this.tag = tag;
        inputCount++;
        preparedX = new double[inputCount];
        preparedX[0] = 1;
        weights = new double[inputCount];
        for(int i = 0; i < inputCount; i++)
            weights[i] = Math.random() - 0.5;
    }

    public int getInputValuesCount() {
        return weights.length;
    }

    public double getInput(int index) {
        return preparedX[index];
    }

    public double getWeight(int index) {
        return weights[index];
    }

    public void correctWeights(int index, double weight) {
        if(index > weights.length) throw new IllegalArgumentException();

        weights[index] = weight;
    }

    public void setInput(double []x) {

        if(preparedX.length != weights.length) throw new RuntimeException();

        for(int i = 1; i < preparedX.length; i++)
            preparedX[i] = x[i - 1];

        y = 0;

        for(int i = 0; i < weights.length; i++)
            y += preparedX[i] * weights[i];

    }

    public double getOutputY() {
        ActivationFunction activationFunction =  new SigmoidFunction();
        return activationFunction.function(y);
    }

    public class SigmoidFunction implements ActivationFunction {

        public static final double ONE = 1.0;
        public static final double ALPHA = 1.0;

        public double function(double y) {
            return ONE / (ONE + Math.exp(- ALPHA * y));
        }
    }
}
