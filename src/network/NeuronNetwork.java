package network;

import neuron.Neuron;

import java.util.Arrays;

/**
 * Created by kucherenko on 04.04.15.
 */

public class NeuronNetwork {

    public static final int LAYER_COUNT = 3;

    private Layer layer = new Layer(LAYER_COUNT);
    private Neuron neuron = new Neuron(LAYER_COUNT, "output neuron");

    public void setInput(double x) {
        double [] inputX = new double[LAYER_COUNT];
        for(int i = 0; i < inputX.length; inputX[i++] = x);

        layer.setInput(inputX);
    }

    public double getOutput() {
        neuron.setInput(layer.getOutput());
        double output =  neuron.getOutputY();
        return output;
    }

    public void train() {

        final double [] x = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
        final double [] expected = {0.000549,
                0.310758,
                0.450369,
                0.57186,
                0.681125,
                0.752747,
                0.815808,
                0.857848,
                0.893805,
                0.928823,
                0.969081};


        for(int i = 0; i < 100_000_000; i++) {

            int index = (int)(Math.random() * 10);

            double input = x[index];

            setInput(input);


            double desiredY = expected[index];
            double y = getOutput();

            double deltaOut = calculateOutDelta(y, desiredY);

            double[] innerDeltas = new double[LAYER_COUNT];

            for(int j = 0; j < LAYER_COUNT; j++) {
                innerDeltas[j] = layer.getDelta(j, deltaOut, neuron.getWeight(j + 1));
            }

            for(int j = 0; j <= LAYER_COUNT; j++) {
                double newWeight = neuron.getWeight(j) + Constants.STEP * deltaOut * neuron.getInput(j);
                neuron.correctWeights(j, newWeight);
            }

            for(int j = 0; j < LAYER_COUNT; j++) {
               layer.correctWeight(j, innerDeltas[j]);
            }

        }

    }

    private double calculateOutDelta(double out, double desired) {
        return  out * (1 - out) * (desired - out);
    }
}
