package network;

import neuron.Neuron;

/**
 * Created by kucherenko on 04.04.15.
 */

public class Layer {

    private Neuron[] neurons;

    private double [] outputs;

    public Layer(int count) {
        neurons = new Neuron[count];
        outputs = new double[count];
        for(int i = 0;  i < count; i++)
            neurons[i] = new Neuron(1, " middle  neron " + i);
    }

    public double getDelta(int index, double delta, double weight) {
        return outputs[index] * ( 1 - outputs[index]) * delta * weight;
    }

    public void setInput(double[] x) {
        for(int i = 0; i < neurons.length; neurons[i++].setInput(x));
    }

    public double[] getOutput() {
        for(int i = 0; i < neurons.length; i++)
            outputs[i] = neurons[i].getOutputY();
        return outputs;
    }

    public void correctWeight(int index, double delta) {
        for(int k = 0; k < neurons[index].getInputValuesCount(); k++)
            neurons[index].correctWeights(k, neurons[index].getWeight(k) + Constants.STEP * delta * neurons[index].getInput(k));
    }

}
